from django.db import models

class addQuestion(models.Model):
    CHOICES = (
    ('a','A'),
    ('b','B'),
    ('c','C'),
    ('d','D')
    )

    question = models.CharField(max_length=500)
    option1 = models.CharField(max_length = 250)
    option2 = models.CharField(max_length = 250)
    option3 = models.CharField(max_length = 250)
    option4 = models.CharField(max_length = 250)
    answer = models.CharField(max_length= 1, choices = CHOICES, blank=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return self.question

# Create your models here.
class feedback(models.Model):
    name= models.CharField(max_length=200)
    email= models.EmailField(max_length=250)
    message = models.TextField()
    Date = models.DateTimeField(auto_now_add=True, blank = True)


    def __str__(self):
        return self.message

    class Meta:
        verbose_name_plural = 'Feedback'

class scoreboard(models.Model):
    Name = models.CharField(max_length=200, blank=True)
    Right_Answers= models.CharField(max_length=200)
    Wrong_Answers= models.CharField(max_length=250)
    Date = models.DateTimeField(auto_now_add=True, blank = True)

    def __str__(self):
        return repr(self.Date)

    class Meta:
        verbose_name_plural = 'Score Board'

class add_fact(models.Model):
    fact = models.TextField()
    author_name = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add= True, blank = True)

    def __str__(self):
        return self.fact
