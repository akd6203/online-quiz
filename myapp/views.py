from django.shortcuts import render
from myapp.models import feedback, scoreboard, addQuestion, add_fact
from django.http import HttpResponse, HttpResponseRedirect
import requests
# Create your views here.
def index(request):
    if request.method =='POST':
        name = request.POST['name']
        email=request.POST['email']
        message=request.POST['message']
        feedbackobj = feedback(name=name,email=email,message=message)
        feedbackobj.save()
        print('data saved')

    return render(request, 'index.html')

def quiz(request):
    data = addQuestion.objects.order_by('created_at')
    right = 0
    wrong = 0
    data1 = []
    result = {}
    if request.method == 'POST':
        print(request.POST)
        for m in data:
            if request.POST['q'+repr(m.id)] == request.POST['answer'+repr(m.id)]:
                rr = {'status':'green','id':m.id,'question':m.question,'option1':m.option1,'option2':m.option2,'option3':m.option3,'option4':m.option4,'answer':m.answer,'useranswer':request.POST['q'+repr(m.id)],'description':m.description}
                data1.append(rr)
                right += 1
            else:
                rr = {'status':'red','id':m.id,'question':m.question,'option1':m.option1,'option2':m.option2,'option3':m.option3,'option4':m.option4,'answer':m.answer,'useranswer':request.POST['q'+repr(m.id)],'description':m.description}
                data1.append(rr)
                wrong += 1
        #print(data1)
        result['right'] = right;
        result['wrong'] = wrong;

        board = scoreboard.objects.order_by('Date')
        return render(request,'result.html',{'d':data,'d1':data1,'quiz':result,'board':board})


    return render(request,'quiz.html',{'d':data})
    # api = requests.get('http://sachtechsolution.com/dailydose/admin/webservice/questionsProcess.php?type=questions')
    # mydata= api.json()
    # right = 0
    # wrong = 0
    # result = {'myans':'active'}
    # count = 1
    # mynewdata = []
    # for q in mydata['Questions']:
    #
    #     dic = {'id':count,'question':q['question'],'qid':q['ques_id'],'a':q['option1'],'b':q['option2'],'c':q['option3'],'d':q['option4']}
    #     mynewdata.append(dic)
    #     count += 1
    # #print(mynewdata)
    # for a in mydata['Questions']:
    #     if request.method == 'POST' and request.POST:
    #         if a['answer'] == request.POST[a['ques_id']]:
    #             right += 1
    #         else:
    #             wrong += 1
    #
    #
    # result['right'] = right;
    # result['wrong'] = wrong;
    # print(result)
    # if result['right'] == 0 or result['wrong'] == 0:
    #     return render(request, 'quiz.html',context = {'quiz':mynewdata})
    # else:
    #     scores = scoreboard(Right_Answers=right, Wrong_Answers=wrong)
    #     scores.save()
    #     print('saved successfully')
    #     board = scoreboard.objects.order_by('Date')
    #
    #     return render(request, 'result.html',context = {'quiz':result,'des':mydata,'board':board})

def facts(request):
    data = add_fact.objects.order_by('date')
    return render(request, 'fact.html',{'facts':data})

#     apfact = requests.get('http://sachtechsolution.com/dailydose/admin/webservice/factProcess.php?type=getLanguageFacts&languageId=3')
#     data =apfact.json()
#     count = 1
#     mynewfact = []
#     for r in data['facts']:
#
#         dicfact = {'id':count,'thought':r['thought']}
#         mynewfact.append(dicfact)
#         count += 1
#     #print(mynewfact)
#     return render(request,'fact.html',context = {'fact':mynewfact})
def insertdata(request):
    if request.method == 'POST' and request.POST:
        m = request.POST['name']
        n = request.POST['right']
        o = request.POST['wrong']
        return HttpResponse(request.POST['name'])
    else:
        return HttpResponse('index hello')

def about(request):
    return render(request,'about.html')

def board(request):
    if request.method == 'POST':
        name = request.POST['nm']
        rt = request.POST['r']
        wr = request.POST['w']

        scores = scoreboard(Name= name, Right_Answers=rt, Wrong_Answers=wr)
        scores.save()
        print("Saved!!!")
    board = scoreboard.objects.order_by('Date')
    return render(request,'board.html',{'board':board})
