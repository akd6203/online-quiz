from django.contrib import admin
from myapp.models import feedback,scoreboard,addQuestion,add_fact
# Register your models here.

class feedbackAdmin(admin.ModelAdmin):
    fields =['email' , 'name' , 'message']

    search_fields = ['email' , 'name' , 'message']

    list_filter = ['email' ]

    list_display = ['email' , 'name' , 'message']

    #list_editable = [ 'message']

class scoreboardAdmin(admin.ModelAdmin):
    fields =['Right_Answers','Wrong_Answers','Date']

    search_fields = ['Right_Answers','Wrong_Answers','Date']

    list_filter = ['Right_Answers','Wrong_Answers','Date' ]

    list_display = ['Right_Answers','Wrong_Answers','Date']

    readonly_fields = ('Date',)

    # list_editable = [ 'Date']
class addQuestionAdmin(admin.ModelAdmin):

    search_fields = ['question',]

    list_filter = ['question','description','created_at' ]

    list_display = ['question','description','created_at']

    readonly_fields = ('created_at',)

class add_factAdmin(admin.ModelAdmin):

    search_fields = ['fact','author_name']

    list_filter = ['fact','author_name','date' ]

    list_display = ['fact','author_name','date']

    readonly_fields = ('date',)

admin.site.register(feedback,feedbackAdmin)
admin.site.register(scoreboard, scoreboardAdmin)
admin.site.register(addQuestion, addQuestionAdmin )
admin.site.register(add_fact , add_factAdmin )
