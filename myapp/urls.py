from django.conf.urls import url
from myapp import views
app_name ='quiz'
urlpatterns = [
    url('question/',views.quiz, name='question'),
    #url('result/',views.result, name='result'),
    url('facts/',views.facts, name='facts'),
    url('insertdata/',views.insertdata, name='insertdata'),
    url('about/',views.about, name='aboutdev'),
    url('board/',views.board, name='aboutdev'),

]
